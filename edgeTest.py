import cv2
import numpy as np

def auto_canny(image, sigma=0.33):
    # compute the median of the single channel pixel intensities
    v = np.median(image)

    # apply automatic Canny edge detection using the computed median
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)

    # return the edged image
    return edged


def detectSquare(contour):
    #shape = "unidentified"

    peri = cv2.arcLength(contour, True)

    approx = cv2.approxPolyDP(contour, .04 * peri, True)

    if len(approx == 4):
        rectTuple = cv2.boundingRect(approx)

        return rectTuple
        #_,_,w,h = rectTuple
        #ar = w / float(h)
        #if ar > .95 and ar < 1.05:
            #return rectTuple

            #print("Square Found!")
    return None

def calculate_channel_similarness(b,g,r):
    channel_average = float(b+g+r)/3.0
    return abs(b-channel_average) + abs(g-channel_average) + abs(r-channel_average)


kernel = np.ones((5,5),np.uint8)
scaleFactor = 1
cap = cv2.VideoCapture(0)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    frame_resized = cv2.resize(frame, None, fx=1.0/scaleFactor, fy=1.0/scaleFactor)

    gray = cv2.cvtColor(frame_resized, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (3, 3), 0)

    # apply Canny edge detection using a wide threshold, tight
    # threshold, and automatically determined threshold
    #wide = cv2.Canny(blurred, 10, 200)
    tight = cv2.Canny(blurred, 225, 250)
    tight = cv2.dilate(tight, kernel)
    #auto = auto_canny(blurred)

    contours, hierarchy = cv2.findContours(tight, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)   #NONE

    for i in range(len(contours)):
        found = detectSquare(contours[i])
        if found != None:

            x, y, w, h = found

            #for x_pixel in range(x, x+w):
                #for y_pixel in range(y, y+h):
                    #b = frame_resized[x_pixel, y_pixel, 0]
                    #g = frame_resized[x_pixel, y_pixel, 1]
                    #r = frame_resized[x_pixel, y_pixel, 2]



            cv2.rectangle(frame_resized, found, (255,255,0), 8)
        cv2.drawContours(frame_resized, contours, i, (255,255,255), 1, 1, hierarchy)
    # show the images
    cv2.imshow("Original", cv2.resize(frame_resized, None, fx=scaleFactor, fy=scaleFactor))
    #cv2.imshow("contours", cv2.resize(np.hstack([wide, tight, auto]), None, fx=scaleFactor, fy=scaleFactor))
    cv2.imshow("Edges", cv2.resize(tight, None, fx=scaleFactor, fy=scaleFactor))



    #cv2.imshow('frame', cv2.resize(frame_resized, None, fx=scaleFactor, fy=scaleFactor))
    #cv2.imshow('thresh', cv2.resize(thresh, None, fx=scaleFactor, fy=scaleFactor))
    #cv2.imshow('opening', cv2.resize(opening, None, fx=scaleFactor, fy=scaleFactor))
    #cv2.imshow('')
    #cv2.imshow('gray', gray)
    if cv2.waitKey(30) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
