import cv2
import numpy as np
from pyzbar import pyzbar
import argparse

low_H = 0
low_S = 0
low_V = 0

high_H = 360
high_S = 10
high_V = 100

cap = cv2.VideoCapture(0)

ap = argparse.ArgumentParser()
ap.add_argument("-o", "--output", type=str, default="barcodes.csv", help="path to output CSV file containing barcodes")
args = vars(ap.parse_args())

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()
    """
    rgb_planes = cv2.split(img)

    result_planes = []
    result_norm_planes = []
    for plane in rgb_planes:
        dilated_img = cv2.dilate(plane, np.ones((7,7), np.uint8))
        bg_img = cv2.medianBlur(dilated_img, 21)
        diff_img = 255 - cv2.absdiff(plane, bg_img)
        norm_img = cv2.normalize(diff_img,None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8UC1)
        result_planes.append(diff_img)
        result_norm_planes.append(norm_img)

    result = cv2.merge(result_planes)
    result_norm = cv2.merge(result_norm_planes)



    img_yuv = cv2.cvtColor(frame, cv2.COLOR_BGR2YUV)

    img_yuv[:, :, 0] = cv2.equalizeHist(img_yuv[:, :, 0])

    img_output = cv2.cvtColor(img_yuv, cv2.COLOR_YUV2BGR)

    ####
    """
    testFrame = cv2.inRange(cv2.cvtColor(frame, cv2.COLOR_BGR2HSV), (low_H, low_S, low_V), (high_H, high_S, high_V))

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    gray = np.float32(gray)

    dst = cv2.cornerHarris(gray,2,3,0.04)

    dst = cv2.dilate(dst,None)

    frame[dst>0.01*dst.max()]=[0,0,255]

    ######
    """
    finalFrame = frame
    barcodes = pyzbar.decode(finalFrame)

    for barcode in barcodes:
        (x, y, w, h) = barcode.rect

        cv2.rectangle(finalFrame, (x, y), (x + w, y + h), (0, 0, 255), 2)

        #barcodeData = barcode.data.decode("utf-8")
        #barcodeType = barcode.type

        # draw the barcode data and barcode type on the image
        #text = "{} ({})".format(barcodeData, barcodeType)
        #cv2.putText(frame, text, (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)

        #print("[INFO] Found {} barcode: {}".format(barcodeType, barcodeData))

    # Our operations on the frame come here
    #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Display the resulting frame

    """
    #cv2.imshow('output', img_output)
    cv2.imshow('yuv', frame)
    #cv2.imshow('gray', gray)
    if cv2.waitKey(30) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
