from ar_markers import detect_markers
import cv2
import numpy as np
import time

import argparse

cap = cv2.VideoCapture(0)

_, testFrame = cap.read()

width = len(testFrame[0])
height = len(testFrame)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    frame_resized = cv2.resize(frame, None, fx=.5, fy=.5)

    image = cv2.cvtColor(frame_resized, cv2.COLOR_BGR2GRAY)

    #new_image = np.zeros(image.shape, image.dtype)
    #alpha = 1.5 # Simple contrast control
    #beta = 0    # Simple brightness control


    initial_time = time.time()

    new_image = cv2.bilateralFilter(image, 5, 75, 75);

    #for y in range(image.shape[0]):
    #    for x in range(image.shape[1]):
    #        new_image[y,x] = np.clip(alpha*image[y,x] + beta, 0, 255)
    final_time = time.time()
    print(final_time-initial_time)

    markers = detect_markers(new_image)

    for marker in markers:
        marker.highlite_marker(new_image)

    cv2.imshow('frame', new_image)
    if cv2.waitKey(100) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
