def calculate_channel_similarness(b,g,r):
    channel_average = float(b+g+r)/3.0
    return (abs(b-channel_average) + abs(g-channel_average) + abs(r-channel_average))**2

print(calculate_channel_similarness(255,255,255))
print(calculate_channel_similarness(255,250,251))
print(calculate_channel_similarness(0,0,0))
print(calculate_channel_similarness(10,15,5))
print(calculate_channel_similarness(255,0,25))


import cv2
import numpy as np


cap = cv2.VideoCapture(0)


scaleFactor = 8

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    frame_resized = cv2.resize(frame, None, fx=1.0/scaleFactor, fy=1.0/scaleFactor)

    height = len(frame_resized)
    width = len(frame_resized[0])

    img = np.zeros([height, width, 1], dtype=np.float)

    for x in range(width):
        for y in range(height):
            b = frame_resized[y, x, 0]
            g = frame_resized[y, x, 1]
            r = frame_resized[y, x, 2]
            if calculate_channel_similarness(b,g,r) > 1000:
                img[y, x, 0] = 1


    cv2.imshow("frame_resized", cv2.resize(frame_resized, None, fx=scaleFactor, fy=scaleFactor))
    cv2.imshow("graywhiteblack", cv2.resize(img, None, fx=scaleFactor, fy=scaleFactor))

    if cv2.waitKey(30) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
