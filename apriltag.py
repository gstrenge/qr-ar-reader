import cv2
import numpy as np

def auto_canny(image, sigma=0.33):
# compute the median of the single channel pixel intensities
    v = np.median(image)

# apply automatic Canny edge detection using the computed median
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)

# return the edged image
    return edged


kernel = np.ones((5,5),np.uint8)
scaleFactor = 1
cap = cv2.VideoCapture(0)

while(True):
        # Capture frame-by-frame
        ret, frame = cap.read()

        frame_resized = cv2.resize(frame, None, fx=1.0/scaleFactor, fy=1.0/scaleFactor)

        gray = cv2.cvtColor(frame_resized, cv2.COLOR_BGR2GRAY)
        blurred = cv2.GaussianBlur(gray, (3, 3), 0)

        # apply Canny edge detection using a wide threshold, tight
        # threshold, and automatically determined threshold
        #wide = cv2.Canny(blurred, 10, 200)
        tight = cv2.Canny(blurred, 225, 250)
        #tight = cv2.dilate(tight, kernel)





        img = frame_resized
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        blurred = cv2.GaussianBlur(gray, (3, 3), 0)
        edges = cv2.Canny(blurred,50,150,apertureSize = 3)

        minLineLength = 100
        maxLineGap = 25
        lines = cv2.HoughLinesP(edges,1,np.pi/180,100)
        for x1,y1,x2,y2 in lines[0]:
            cv2.line(img,(x1,y1),(x2,y2),(0,255,0),2)


        cv2.imshow("img", cv2.resize(img, None, fx=scaleFactor, fy=scaleFactor))
        cv2.imshow("edges", cv2.resize(edges, None, fx=scaleFactor, fy=scaleFactor))
        #cv2.imshow("edges", cv2.resize(img, None, fx=scaleFactor, fy=scaleFactor))



#cv2.imshow('frame', cv2.resize(frame_resized, None, fx=scaleFactor, fy=scaleFactor))
#cv2.imshow('thresh', cv2.resize(thresh, None, fx=scaleFactor, fy=scaleFactor))
#cv2.imshow('opening', cv2.resize(opening, None, fx=scaleFactor, fy=scaleFactor))
#cv2.imshow('')
#cv2.imshow('gray', gray)
        if cv2.waitKey(30) & 0xFF == ord('q'):
            break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
