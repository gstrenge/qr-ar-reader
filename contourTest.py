import cv2
import numpy as np


cap = cv2.VideoCapture(0)

low_H = 0
low_S = 0
low_V = 126

high_H = 180
high_S = 71
high_V = 255

kernel = np.ones((5,5),np.uint8)

scaleFactor = 4

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    frame_resized = cv2.resize(frame, None, fx=1.0/scaleFactor, fy=1.0/scaleFactor)

    #print(len(frame))

    frame_HSV = cv2.cvtColor(frame_resized, cv2.COLOR_BGR2HSV)

    thresh = cv2.inRange(frame_HSV, (low_H, low_S, low_V), (high_H, high_S, high_V))

    #thresh2 = cv2.erode()

    opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)

    contours, hierarchy = cv2.findContours(opening, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    maxArea = 0
    maxAreaIndex = 0

    hulls=[]

    for i in range(len(contours)):

        area = cv2.contourArea(contours[i])

        if area>maxArea:
            maxArea=area
            maxAreaIndex = i

        if area < 1000:
            continue
        cv2.drawContours(frame_resized, contours, i, (255,255,255), 1, 1, hierarchy)

    #print(maxArea)

    hulls.append(cv2.convexHull(contours[maxAreaIndex], False))

    cv2.drawContours(frame_resized, hulls, 0, (255,0,0), 3, 8)

    # Distance Calc
    #print(maxArea)
    if abs(603.2246 - maxArea) < 5:
        pass
        #break


    cv2.imshow('frame', cv2.resize(frame_resized, None, fx=scaleFactor, fy=scaleFactor))
    cv2.imshow('thresh', cv2.resize(thresh, None, fx=scaleFactor, fy=scaleFactor))
    cv2.imshow('opening', cv2.resize(opening, None, fx=scaleFactor, fy=scaleFactor))
    #cv2.imshow('')
    #cv2.imshow('gray', gray)
    if cv2.waitKey(30) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
